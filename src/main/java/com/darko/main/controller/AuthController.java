package com.darko.main.controller;

import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.darko.main.model.Account;
import com.darko.main.model.Genre;
import com.darko.main.model.Movie;
import com.darko.main.model.RootData;
import com.darko.main.model.User;
import com.darko.main.model.UserInfoStats;
import com.darko.main.model.UserPreferences;
import com.darko.main.service.AuthService;

@Controller
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private AuthService authService;

	@Autowired
	private HttpSession httpSession;

	@RequestMapping(value = "/preferences/save", method = RequestMethod.POST)
	public String savePreferences(String genres, String rating, Model model) {

		if(httpSession.getAttribute("username") != null) {
			model.addAttribute("username", httpSession.getAttribute("username"));
		}
		
		model.addAttribute("message", "Preferences saved");

		String plainCreds = httpSession.getAttribute("username") + ":" + httpSession.getAttribute("pass");
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);
		Account acc = new Account(Long.parseLong(httpSession.getAttribute("userId").toString()),
				httpSession.getAttribute("username").toString());

		UserInfoStats userInfoStats = authService.savePreferences(genres, rating, acc, headers, httpSession);

		UserPreferences userPreferences = authService.loadPreferences(acc, headers, httpSession);
		
		model.addAttribute("savedGenre", userPreferences.getTmdbGenres());
		model.addAttribute("savedRating", userPreferences.getTmdbRating());
		String msg = "Preferences saved";
		model.addAttribute("msg", msg);
		return "fragments/preferencesFragment";
	}
	
	@RequestMapping(value = "/preferences", method = RequestMethod.GET)
	public String loadPreferences(Model model) {
		if(httpSession.getAttribute("username") != null) {
			model.addAttribute("username", httpSession.getAttribute("username"));
		}
		String plainCreds = httpSession.getAttribute("username") + ":" + httpSession.getAttribute("pass");
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);
		
		Account acc = new Account(Long.parseLong(httpSession.getAttribute("userId").toString()),
				httpSession.getAttribute("username").toString());

		if((httpSession.getAttribute("username") != null) && (httpSession.getAttribute("pass") != null)) {
			UserPreferences userPreferences = authService.loadPreferences(acc, headers, httpSession);
			if(userPreferences != null) {
				model.addAttribute("savedGenre", userPreferences.getTmdbGenres());
				model.addAttribute("savedRating", userPreferences.getTmdbRating());
			}
		}
		String msg = "Preferences loaded";
		model.addAttribute("msg", msg);
		return "fragments/preferencesFragment";
	}
	
	@RequestMapping(value = "/suggest", method = RequestMethod.POST)
	public String loggedInSuggestion(Model model) {
		if((httpSession.getAttribute("username") != null) && (httpSession.getAttribute("pass") != null)) {
			
			model.addAttribute("username", httpSession.getAttribute("username"));
			
			String plainCreds = httpSession.getAttribute("username") + ":" + httpSession.getAttribute("pass");
			byte[] plainCredsBytes = plainCreds.getBytes();
			byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			String base64Creds = new String(base64CredsBytes);
	
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);
			
			Account acc = new Account(Long.parseLong(httpSession.getAttribute("userId").toString()),
					httpSession.getAttribute("username").toString());
	
			UserPreferences userPreferences = authService.loadPreferences(acc, headers, httpSession);
			
			String genres = "";
			float rating = 6;
			if(userPreferences != null) {
				genres = userPreferences.getTmdbGenres();
				rating = userPreferences.getTmdbRating();
			}
			
			//System.out.println(genres + " | " + rating);
			RootData rootData = authService.loggedInSuggestion(genres, rating + "", acc, headers, httpSession);
			
			UserInfoStats userInfoStats = rootData.getUserInfoStats();
			Movie m = rootData.getMovie();
			if(m != null) {
				checkPosterAvailability(m);
				
				String imageLink = null;
				if(m.getTomatoMeter() != null) {
					if(Float.parseFloat(m.getTomatoMeter()) < 60 ) {
						imageLink = "http://localhost:8080/images/splat.png";
					}
					if((Float.parseFloat(m.getTomatoMeter()) >= 60) && 
							(Float.parseFloat(m.getTomatoMeter()) < 75)) {
						
						imageLink = "http://localhost:8080/images/fresh.png";
					} 
					if(Float.parseFloat(m.getTomatoMeter()) >= 75) {
						imageLink = "http://localhost:8080/images/certified.png";
					}
				}
				
				String imageLink2 = null;
				if(m.getTomatoUserRating() != null) {
					if(Float.parseFloat(m.getTomatoUserRating()) < 3.5){
						imageLink2 = "http://localhost:8080/images/tipped_popcorn.png";
					}
					if(Float.parseFloat(m.getTomatoUserRating()) >= 3.5) {
						imageLink2 = "http://localhost:8080/images/full_popcorn.png";
					}
				}
				
				model.addAttribute("tomatoMeterImage", imageLink);
				model.addAttribute("tomatoUserImage", imageLink2);
				String allGenres = "";
				for(int i=0; i<m.getGenres().size(); i++) {
					allGenres += m.getGenres().get(i).getId();
					if(i < (m.getGenres().size()-1)) {
						allGenres += ",";
					}
				}
				model.addAttribute("allGenres", allGenres);
				model.addAttribute("movie", m);
				return "index";
			}
			else {
				String errorHeader = "";
				String errorMsg = userInfoStats.getMessage();
				model.addAttribute("errorHeader", errorHeader);
				model.addAttribute("errorMsg", errorMsg);
				return "fragments/errorFragment";
			}
		}
		else {
			String errorHeader = "";
			String errorMsg = "";
			model.addAttribute("errorHeader", errorHeader);
			model.addAttribute("errorMsg", errorMsg);
			return "fragments/errorFragment";
		}
	}
	
	@RequestMapping(value = "/addMovie", method = RequestMethod.POST)
	public String addToWatchedList(
			String movieName, 
			String imdbId, 
			Model model) {
		
		if((httpSession.getAttribute("username") != null) && (httpSession.getAttribute("pass") != null)) {
			
			model.addAttribute("username", httpSession.getAttribute("username"));
			
			
			String plainCreds = httpSession.getAttribute("username") + ":" + httpSession.getAttribute("pass");
			byte[] plainCredsBytes = plainCreds.getBytes();
			byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			String base64Creds = new String(base64CredsBytes);
	
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);
			
			Account acc = new Account(Long.parseLong(httpSession.getAttribute("userId").toString()),
					httpSession.getAttribute("username").toString());
			
			UserInfoStats userInfoStats = authService.addToWatchedList(movieName, imdbId, acc, headers, httpSession);
			
			String msg = userInfoStats.getMessage();
			model.addAttribute("msg", msg);
			
			List<String> movies = authService.watchedMovies(headers, httpSession);
			model.addAttribute("movies", movies);
			return "fragments/auth/watchedMovies";
		}
		else {
			
			String msg = "The movie was not added into the watch list because some error happend. Please contact the administrator.";
			model.addAttribute("msg", msg);
			listWatchedMovies(model);
			return "fragments/auth/watchedMovies";
		}
	}
	
	@RequestMapping(value = "/watched", method = RequestMethod.GET)
	public String listWatchedMovies(Model model) {
		if((httpSession.getAttribute("username") != null) && (httpSession.getAttribute("pass") != null)) {
			
			model.addAttribute("username", httpSession.getAttribute("username"));
			
			String plainCreds = httpSession.getAttribute("username") + ":" + httpSession.getAttribute("pass");
			byte[] plainCredsBytes = plainCreds.getBytes();
			byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			String base64Creds = new String(base64CredsBytes);
	
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);
	
			List<String> movies = authService.watchedMovies(headers, httpSession);
			
			model.addAttribute("movies", movies);
			return "fragments/auth/watchedMovies";
		}
		else {
			String msg = "No watched movies";
			model.addAttribute("msg", msg);
			return "fragments/auth/watchedMovies";
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(String username, String password, Model model) {

		User user = new User(username, password);
		UserInfoStats userInfoStats = authService.login(user, httpSession);

		model.addAttribute("username", httpSession.getAttribute("username"));
		model.addAttribute("userId", httpSession.getAttribute("userId"));
		
		String msg = userInfoStats.getMessage();
		
		if(userInfoStats.getStatus() == "failed") {
			model.addAttribute("msg", msg);
			model.addAttribute("status", "failed");
			
			return "fragments/loginFragment";
		}
		else {
			return loggedInSuggestion(model);
		}
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model) {
		authService.logOut(httpSession);
		
		String msg = "The user logged out successfully";
		model.addAttribute("msg", msg);
		return "fragments/loginFragment";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(String username, String password, Model model) {
		
		User user = new User(username, password);
		user.setSalt(new SecureRandom().nextInt(9999) + "");
		UserInfoStats userInfoStats = authService.register(user);

		if(userInfoStats.getStatus().equals("fail")) {
			String msg = userInfoStats.getMessage();
			model.addAttribute("msg", msg);
			model.addAttribute("status", "failed");
			return "fragments/registerFragment";
		}
		else {
			String msg = userInfoStats.getMessage();
			model.addAttribute("msg", msg);
			return "fragments/loginFragment";
		}
	}
	
	private void checkPosterAvailability(Movie m) {
		try {
			String poster = "http://image.tmdb.org/t/p/w500" + m.getPosterURL();
			
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(poster)
					.openConnection();
			con.setRequestMethod("HEAD");
			if (con.getResponseCode() == HttpsURLConnection.HTTP_OK) {
				m.setPosterURL(poster);
			} else {
				m.setPosterURL("http://www.filmfodder.com/reviews/images/poster-not-available.jpg");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
