package com.darko.main.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.darko.main.model.Movie;
import com.darko.main.service.NonAuthService;

@Controller
@RequestMapping("/nonauth")
public class NonAuthController {

	@Autowired
	@Qualifier("NonAuthService")
	private NonAuthService nonAuthService;
	
	@Autowired
	private HttpSession httpSession;

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String searchTitle(String title, Model model) {

		List<Movie> listMovie = nonAuthService.searchMovie(title);
		if (listMovie != null) {
			for (Movie m : listMovie) {
				checkPosterAvailability(m);
			}
			model.addAttribute("movies", listMovie);
			
			if((httpSession.getAttribute("username") != null) && (httpSession.getAttribute("pass") != null)) {
				model.addAttribute("username", httpSession.getAttribute("username"));
			}
			return "index";
		} else {
			return "fragments/error";
		}
	}
	
	@RequestMapping(value = "/movie", method = RequestMethod.POST)
	public String movieDetails(String id, Model model) throws IOException {
		Movie m = null;
		if(id == "") {
			String genre = null;
			String rating = null;
			m = nonAuthService.notLoggedInSuggestion(genre, rating);
		}
		else {
			m = nonAuthService.movieDetails(id);
		}
		checkPosterAvailability(m);

		String imageLink = null;
		if(m.getTomatoMeter() != null) {
			if(Float.parseFloat(m.getTomatoMeter()) < 60 ) {
				imageLink = "http://localhost:8080/images/splat.png";
			}
			if((Float.parseFloat(m.getTomatoMeter()) >= 60) && 
					(Float.parseFloat(m.getTomatoMeter()) < 75)) {
				
				imageLink = "http://localhost:8080/images/fresh.png";
			} 
			if(Float.parseFloat(m.getTomatoMeter()) >= 75) {
				imageLink = "http://localhost:8080/images/certified.png";
			}
		}
		
		String imageLink2 = null;
		if(m.getTomatoUserRating() != null) {
			if(Float.parseFloat(m.getTomatoUserRating()) < 3.5){
				imageLink2 = "http://localhost:8080/images/tipped_popcorn.png";
			}
			if(Float.parseFloat(m.getTomatoUserRating()) >= 3.5) {
				imageLink2 = "http://localhost:8080/images/full_popcorn.png";
			}
		}

		model.addAttribute("tomatoMeterImage", imageLink);
		model.addAttribute("tomatoUserImage", imageLink2);
		model.addAttribute("movie", m);
		
		if((httpSession.getAttribute("username") != null) && (httpSession.getAttribute("pass") != null)) {
			model.addAttribute("username", httpSession.getAttribute("username"));
		}
		
		return "index";
	}
	
	@RequestMapping(value = "/suggest", method = RequestMethod.POST)
	public String movieSuggestion(
			@RequestParam(value = "genres", required = false) String genres, 
			@RequestParam(value = "rating", required = false) String rating, Model model) {
		
		Movie m = nonAuthService.notLoggedInSuggestion(genres, rating);
		checkPosterAvailability(m);
		
		String imageLink = null;
		if(m.getTomatoMeter() != null) {
			if(Float.parseFloat(m.getTomatoMeter()) < 60 ) {
				imageLink = "http://localhost:8080/images/splat.png";
			}
			if((Float.parseFloat(m.getTomatoMeter()) >= 60) && 
					(Float.parseFloat(m.getTomatoMeter()) < 75)) {
				
				imageLink = "http://localhost:8080/images/fresh.png";
			} 
			if(Float.parseFloat(m.getTomatoMeter()) >= 75) {
				imageLink = "http://localhost:8080/images/certified.png";
			}
		}
		
		String imageLink2 = null;
		if(m.getTomatoUserRating() != null) {
			if(Float.parseFloat(m.getTomatoUserRating()) < 3.5){
				imageLink2 = "http://localhost:8080/images/tipped_popcorn.png";
			}
			if(Float.parseFloat(m.getTomatoUserRating()) >= 3.5) {
				imageLink2 = "http://localhost:8080/images/full_popcorn.png";
			}
		}

		model.addAttribute("tomatoMeterImage", imageLink);
		model.addAttribute("tomatoUserImage", imageLink2);
		
		model.addAttribute("movie", m);
		
		if((httpSession.getAttribute("username") != null) && (httpSession.getAttribute("pass") != null)) {
			model.addAttribute("username", httpSession.getAttribute("username"));
		}
		
		return "index";
	}
	
	private void checkPosterAvailability(Movie m) {
		try {
			String poster = "http://image.tmdb.org/t/p/w500" + m.getPosterURL();
			
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(poster)
					.openConnection();
			con.setRequestMethod("HEAD");
			if (con.getResponseCode() == HttpsURLConnection.HTTP_OK) {
				m.setPosterURL(poster);
			} else {
				m.setPosterURL("http://www.filmfodder.com/reviews/images/poster-not-available.jpg");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
