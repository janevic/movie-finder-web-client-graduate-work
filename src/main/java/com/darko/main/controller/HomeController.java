package com.darko.main.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

	@Autowired
	private NonAuthController nonAuthController;
	
	@Autowired
	private AuthController authController;
	
	@Autowired
	private HttpSession httpSession;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model m) throws IOException {
		if(httpSession.getAttribute("username") != null) {
			m.addAttribute("username", httpSession.getAttribute("username"));
			authController.loggedInSuggestion(m);
		}
		else {
			nonAuthController.movieDetails("", m);
		}
		
		return "index";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model m) {
		return "fragments/loginFragment";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model m) {
		
		return "fragments/registerFragment";
	}
	
}
