package com.darko.main.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchMovie {

	private List<Movie> movies;

	public SearchMovie() {
		
	}
	
	public SearchMovie(List<Movie> movies) {
		super();
		this.movies = movies;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}
	
}
