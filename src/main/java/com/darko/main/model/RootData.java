package com.darko.main.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RootData {

	@JsonProperty("userInfoStats")
	private UserInfoStats userInfoStats;

	@JsonProperty("suggestedMovie")
	private Movie movie;

	public RootData() {
		super();
	}

	public RootData(UserInfoStats userInfoStats, Movie movie) {
		super();
		this.userInfoStats = userInfoStats;
		this.movie = movie;
	}

	public UserInfoStats getUserInfoStats() {
		return userInfoStats;
	}

	public Movie getMovie() {
		return movie;
	}
}
