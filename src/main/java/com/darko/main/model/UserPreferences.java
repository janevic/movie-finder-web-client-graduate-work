package com.darko.main.model;

public class UserPreferences {

	private Long id;
	private float tmdbRating;
	private float rottenTomatoesRating;
	private String tmdbGenres;

	public UserPreferences() {
		super();
	}

	public UserPreferences(float tmdbRating, String tmdbGenres) {
		super();
		this.tmdbRating = tmdbRating;
		this.tmdbGenres = tmdbGenres;
	}

	public UserPreferences(Long id, float tmdbRating, float rottenTomatoesRating, String tmdbGenres) {
		super();
		this.id = id;
		this.tmdbRating = tmdbRating;
		this.rottenTomatoesRating = rottenTomatoesRating;
		this.tmdbGenres = tmdbGenres;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public float getTmdbRating() {
		return tmdbRating;
	}

	public void setTmdbRating(float tmdbRating) {
		this.tmdbRating = tmdbRating;
	}

	public float getRottenTomatoesRating() {
		return rottenTomatoesRating;
	}

	public void setRottenTomatoesRating(float rottenTomatoesRating) {
		this.rottenTomatoesRating = rottenTomatoesRating;
	}

	public String getTmdbGenres() {
		return tmdbGenres;
	}

	public void setTmdbGenres(String tmdbGenres) {
		this.tmdbGenres = tmdbGenres;
	}

}
