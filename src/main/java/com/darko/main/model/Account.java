package com.darko.main.model;

public class Account {

	private Long accountId;
	private String username;
	
	public Account() {
		super();
	}

	public Account(Long accountId, String username) {
		super();
		this.accountId = accountId;
		this.username = username;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
