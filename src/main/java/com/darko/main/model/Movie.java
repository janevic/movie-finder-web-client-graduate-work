package com.darko.main.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie {

	@JsonProperty("imdbId")
	private String imdbId;
	
	@JsonProperty("theMovieDbId")
	private String theMovieDbId;
	
	@JsonProperty("title")
	private String title;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("youTubeURLTrailers")
	private List<String> youTubeURLTrailers;
	
	@JsonProperty("youTubeURLMovie")
	private String youTubeURLMovie;
	
	@JsonProperty("posterURL")
	private String posterURL;
	
	@JsonProperty("genres")
	private List<Genre> genres;
	
	@JsonProperty("budget")
	private String budget;
	
	@JsonProperty("homepage")
	private String homepage;
	
	@JsonProperty("language")
	private String language;
	
	@JsonProperty("popularity")
	private String popularity;
	
	@JsonProperty("releaseDate")
	private String releaseDate;
	
	@JsonProperty("revenue")
	private String revenue;
	
	@JsonProperty("runtime")
	private String runtime;
	
	@JsonProperty("voteAverageTMDB")
	private String voteAverageTMDB;
	
	@JsonProperty("voteCountTMDB")
	private String voteCountTMDB;
	
	@JsonProperty("director")
	private List<String> director;
	
	@JsonProperty("writer")
	private List<String> writer;
	
	@JsonProperty("actors")
	private List<String> actors;
	
	@JsonProperty("country")
	private String country;
	
	@JsonProperty("awards")
	private String awards;
	
	@JsonProperty("metascore")
	private String metascore;
	
	@JsonProperty("imdbRating")
	private String imdbRating;
	
	@JsonProperty("imdbVotes")
	private String imdbVotes;
	
	@JsonProperty("tomatoMeter")
	private String tomatoMeter;
	
	@JsonProperty("tomatoRating")
	private String tomatoRating;
	
	@JsonProperty("tomatoReviews")
	private String tomatoReviews;
	
	@JsonProperty("tomatoFresh")
	private String tomatoFresh;
	
	@JsonProperty("tomatoRotten")
	private String tomatoRotten;
	
	@JsonProperty("tomatoConsensus")
	private String tomatoConsensus;
	
	@JsonProperty("tomatoUserMeter")
	private String tomatoUserMeter;
	
	@JsonProperty("tomatoUserRating")
	private String tomatoUserRating;
	
	@JsonProperty("tomatoUserReviews")
	private String tomatoUserReviews;
	
	@JsonProperty("tomatoURL")
	private String tomatoURL;

	public Movie() {
		super();
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getTheMovieDbId() {
		return theMovieDbId;
	}

	public void setTheMovieDbId(String theMovieDbId) {
		this.theMovieDbId = theMovieDbId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getYouTubeURLTrailers() {
		return youTubeURLTrailers;
	}

	public void setYouTubeURLTrailers(List<String> youTubeURLTrailers) {
		this.youTubeURLTrailers = youTubeURLTrailers;
	}

	public String getYouTubeURLMovie() {
		return youTubeURLMovie;
	}

	public void setYouTubeURLMovie(String youTubeURLMovie) {
		this.youTubeURLMovie = youTubeURLMovie;
	}

	public String getPosterURL() {
		return posterURL;
	}

	public void setPosterURL(String posterURL) {
		this.posterURL = posterURL;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPopularity() {
		return popularity;
	}

	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public String getVoteAverageTMDB() {
		return voteAverageTMDB;
	}

	public void setVoteAverageTMDB(String voteAverageTMDB) {
		this.voteAverageTMDB = voteAverageTMDB;
	}

	public String getVoteCountTMDB() {
		return voteCountTMDB;
	}

	public void setVoteCountTMDB(String voteCountTMDB) {
		this.voteCountTMDB = voteCountTMDB;
	}

	public List<String> getDirector() {
		return director;
	}

	public void setDirector(List<String> director) {
		this.director = director;
	}

	public List<String> getWriter() {
		return writer;
	}

	public void setWriter(List<String> writer) {
		this.writer = writer;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	public String getAwards() {
		return awards;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMetascore() {
		return metascore;
	}

	public void setMetascore(String metascore) {
		this.metascore = metascore;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	public String getImdbVotes() {
		return imdbVotes;
	}

	public void setImdbVotes(String imdbVotes) {
		this.imdbVotes = imdbVotes;
	}

	public String getTomatoMeter() {
		return tomatoMeter;
	}

	public void setTomatoMeter(String tomatoMeter) {
		this.tomatoMeter = tomatoMeter;
	}

	public String getTomatoRating() {
		return tomatoRating;
	}

	public void setTomatoRating(String tomatoRating) {
		this.tomatoRating = tomatoRating;
	}

	public String getTomatoReviews() {
		return tomatoReviews;
	}

	public void setTomatoReviews(String tomatoReviews) {
		this.tomatoReviews = tomatoReviews;
	}

	public String getTomatoFresh() {
		return tomatoFresh;
	}

	public void setTomatoFresh(String tomatoFresh) {
		this.tomatoFresh = tomatoFresh;
	}

	public String getTomatoRotten() {
		return tomatoRotten;
	}

	public void setTomatoRotten(String tomatoRotten) {
		this.tomatoRotten = tomatoRotten;
	}

	public String getTomatoConsensus() {
		return tomatoConsensus;
	}

	public void setTomatoConsensus(String tomatoConsensus) {
		this.tomatoConsensus = tomatoConsensus;
	}

	public String getTomatoUserMeter() {
		return tomatoUserMeter;
	}

	public void setTomatoUserMeter(String tomatoUserMeter) {
		this.tomatoUserMeter = tomatoUserMeter;
	}

	public String getTomatoUserRating() {
		return tomatoUserRating;
	}

	public void setTomatoUserRating(String tomatoUserRating) {
		this.tomatoUserRating = tomatoUserRating;
	}

	public String getTomatoUserReviews() {
		return tomatoUserReviews;
	}

	public void setTomatoUserReviews(String tomatoUserReviews) {
		this.tomatoUserReviews = tomatoUserReviews;
	}

	public String getTomatoURL() {
		return tomatoURL;
	}

	public void setTomatoURL(String tomatoURL) {
		this.tomatoURL = tomatoURL;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}
	
}
