package com.darko.main.service;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.darko.main.model.Account;
import com.darko.main.model.RootData;
import com.darko.main.model.User;
import com.darko.main.model.UserInfoStats;
import com.darko.main.model.UserPreferences;

@Service("AuthService")
public class AuthService {

	@Value("${serviceUrl}")
	private String serviceUrl;

	public UserInfoStats savePreferences(String genres, String rating, Account acc, HttpHeaders headers,
			HttpSession httpSession) {

		RestTemplate template = new RestTemplate();

		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl + "/auth/user/preferences");
		
		JSONObject jsonBody = new JSONObject();
		if (genres != null) {
			jsonBody.put("genres", genres);
		}
		if (rating != null) {
			jsonBody.put("rating", rating);
		}
		URI targetUrl = url.build().toUri();
		headers.set("Content-Type", "application/json");

		HttpEntity<Object> request = new HttpEntity<Object>(jsonBody.toString(), headers);
		ResponseEntity<UserInfoStats> userInfoStats = template.exchange(targetUrl, HttpMethod.PUT, request,
				UserInfoStats.class);
		return userInfoStats.getBody();
	}

	public UserInfoStats login(User user, HttpSession httpSession) {
		RestTemplate template = new RestTemplate();
		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl + "/user/validity");

		URI targetUrl = url.build().toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");

		String plainCreds = user.getUsername() + ":" + user.getPassword();
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		headers.add(HttpHeaders.AUTHORIZATION, "Basic " + base64Creds);

		HttpEntity<Object> request = new HttpEntity<Object>(headers);
		try {
			ResponseEntity<UserInfoStats> userInfoStats = template.exchange(targetUrl, HttpMethod.POST, request,
					UserInfoStats.class);
			
			httpSession.setAttribute("username", userInfoStats.getBody().getUsername());
			httpSession.setAttribute("userId", userInfoStats.getBody().getUser_id());
			httpSession.setAttribute("pass", user.getPassword());
	
			return userInfoStats.getBody();
		} catch (HttpClientErrorException error)
		{
			System.out.println(error.getResponseBodyAsString());
			UserInfoStats errorStats = new UserInfoStats();
			errorStats.setStatus("failed");
			errorStats.setMessage("The username or password is wrong");
			return errorStats;
		}
		
	}

	public String logOut(HttpSession httpSession) {
		
		httpSession.removeAttribute("username");
		httpSession.removeAttribute("userId");
		httpSession.removeAttribute("pass");
		
		httpSession.invalidate();

		return "User is logged out";
	}
	
	public UserInfoStats register(User user) {
		
		RestTemplate template = new RestTemplate();
		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl + "/user/register");
		
		URI targetUrl = url.build().toUri();
		
		HttpEntity<Object> request = new HttpEntity<Object>(user);
		
		ResponseEntity<UserInfoStats> userInfoStats = template.exchange(targetUrl, HttpMethod.POST, request,
				UserInfoStats.class);
		
		return userInfoStats.getBody();
	}

	public UserPreferences loadPreferences(Account acc, HttpHeaders headers, HttpSession httpSession) {
		RestTemplate template = new RestTemplate();
		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl + "/auth/user/preferences");

		URI targetUrl = url.build().toUri();
		headers.set("Content-Type", "application/json");
		
		HttpEntity<Object> request = new HttpEntity<Object>(acc, headers);

		ResponseEntity<UserPreferences> userInfoStats = template.exchange(targetUrl, HttpMethod.GET, request,
				UserPreferences.class);

		return userInfoStats.getBody();
	}
	
	public RootData loggedInSuggestion(String genres, String rating, Account acc, HttpHeaders headers, HttpSession httpSession) {
		
		RestTemplate template = new RestTemplate();
		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl + "/auth/movieSuggestion");
		if (genres != null) {
			url.queryParam("genres", genres);
		}
		if (rating != null) {
			url.queryParam("rating", rating);
		}
		
		URI targetUrl = url.build().toUri();
		headers.set("Content-Type", "application/json");
		
		HttpEntity<Object> request = new HttpEntity<Object>(acc, headers);
		
		ResponseEntity<RootData> rootData = template.exchange(targetUrl, HttpMethod.GET, request,
				RootData.class);
		
		return rootData.getBody();
	}
	
	public UserInfoStats addToWatchedList(String movieName, String imdbId, Account acc, HttpHeaders headers, HttpSession httpSession) {
		
		RestTemplate template = new RestTemplate();
		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl + "/auth/watched");
		//url.queryParam("movieName", movieName);
		//url.queryParam("imdbId", imdbId);
		JSONObject jsonBody = new JSONObject();
		jsonBody.put("movieName", movieName);
		jsonBody.put("imdbId", imdbId);
		
		URI targetUrl = url.build().toUri();
		//System.out.println(targetUrl.toString());
		headers.set("Content-Type", "application/json");
		
		HttpEntity<Object> request = new HttpEntity<Object>(jsonBody.toString(), headers);

		ResponseEntity<UserInfoStats> userInfoStats = template.exchange(targetUrl, HttpMethod.PUT, request,
				UserInfoStats.class);

		return userInfoStats.getBody();
	}
	
	public List<String> watchedMovies(HttpHeaders headers, HttpSession httpSession) {
		RestTemplate template = new RestTemplate();
		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl + "/auth/watched");
		
		URI targetUrl = url.build().toUri();
		//System.out.println(targetUrl.toString());
		headers.set("Content-Type", "application/json");
		
		HttpEntity<Object> request = new HttpEntity<Object>(headers);
		
		ResponseEntity<String[]> watchedMovies = template.exchange(targetUrl, HttpMethod.GET, request, String[].class);
		
		return Arrays.asList(watchedMovies.getBody());
	}

}
