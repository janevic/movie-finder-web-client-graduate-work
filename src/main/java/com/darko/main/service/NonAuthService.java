package com.darko.main.service;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.darko.main.model.Movie;

@Service("NonAuthService")
public class NonAuthService {

	@Value("${serviceUrl}")
	private String serviceUrl;
	
	public Movie notLoggedInSuggestion(String genres, String rating) {
		RestTemplate template = new RestTemplate();
		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl+"/nonauth/movieSuggestion");
		if(genres != null) {
			url.queryParam("genres", genres);
		}
		if(rating != null) {
			url.queryParam("rating", rating);
		}
		
		URI targetUrl = url.build().toUri();
		
		Movie movie = template.getForObject(targetUrl, Movie.class);
		return movie;
	}
	
	public List<Movie> searchMovie(String title) {
		RestTemplate template = new RestTemplate();
		
		UriComponentsBuilder url = UriComponentsBuilder.fromUriString(serviceUrl+"/nonauth/title");
		if(title != null) {
			url.queryParam("query", title);
			
		}
		else {
			return null;
		}
		URI targetUrl = url.build().toUri();
		Movie[] movies = template.getForObject(targetUrl, Movie[].class);
		
		return Arrays.asList(movies);
	}
	
	public Movie movieDetails(String id) {
		RestTemplate template = new RestTemplate();
		
		URI targetUrl = UriComponentsBuilder.fromUriString(serviceUrl+"/nonauth/movieDetails").queryParam("id", id).build().toUri();

		Movie movie = template.getForObject(targetUrl, Movie.class);
		return movie;
	}
	
}
